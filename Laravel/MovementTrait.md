# Registro de movimientos de un modelo

## Generar el Trait

En la carpeta *app/Traits/* generar el archivo *MovementTrait*

```php
<?php

namespace App\Traits;

use Carbon\Carbon;
use DateTimeZone;
use Exception;

trait MovementTrait
{

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $original = collect((array) $model->getOriginal());
            $changes = collect($model->getAttributes());
            $diff =  $original->diff($changes);
            $movement = $model->movements()->create([
                'user_id' => auth()->user()->id,
                'params' => $diff->toJson(),
            ]);
        });

        static::created(function ($model) {
            $movement = $model->movements()->create([
                'user_id' => auth()->user()->id,
                'notes' => 'Valor inicial',
                'params' => $model->toJson(),
            ]);
        });
    }
}
```

## Generar el modelo y la migración

Ejecutar el siguiente comando dentro de la carpeta de laravel

```bash
php artisan make:model -m Movement
```

editar el archivo *database/migrations/...create_movements_table.php*

```php

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->bigInteger("itemable_id");
            $table->string("itemable_type");

            $table->string('notes')->nullable();
            $table->json('params')->nullable();

            $table->foreign('user_id')->references('id')->on('employees')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}

```

editar el modelo *app/Models/Movement.php*

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        "itemable_id", // del modelo
        "itemable_type", // MODELO
        'notes', // comentario que defina el cambio
        'params' // cambios o datos adicionales
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

```

Correr el siguiente comando

```sh
php artisan migrate
```

## Agregando el movimiento al modelo a revisar

```php
<?php
// TODO: NUEVO
namespace App\Models;

use App\Models\Movement;
use Illuminate\Database\Eloquent\Model;
use App\Traits\MovementTrait;

class Example extends Model
{
    use MovementTrait;

    ...

    public function movements()
    {
        return $this->morphMany(Movement::class, 'itemable');
    }

}

```

[Regresar](../README.md)