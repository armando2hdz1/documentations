# APP CORE

# Desarrollo en Windows con WSL Ubuntu 20.04

## Instalar WSL 

Abrimos `Control Panel > Programs > Turn Windows features on or off` habilitamos las opciones de  `Windows Subsystem for Linux` y `Virtual Machine Platform`

![ejemplo](./img/sagi_local_wsl_01.png)

Nos pedira reiniciar el equipo, una vez que reinicie abrimos la terminal de windows (cmd o Windows Power Shell) y ejecutamos el siguiente comando

```cmd
wsl.exe --install -d Ubuntu-20.04
```

al terminar de instalar nos abrira una nueva ventana donde nos pedira un nombre de usuario y contraseña.

![ejemplo](./img/sagi_local_wsl_02.png)

Despues de poner nuestro usuario y contraseña, nos aparecera el prompt de ubunto, aqui deberemos actualizar los paquetes de ubuntu por primera vez con el siguiente comando

```sh
sudo apt update -y && sudo apt upgrade -y
```

ahora procederemos a instalar los paquetes del proyecto, ejecutamos:

```sh
sudo apt install -y ssh apache2 software-properties-common 
sudo add-apt-repository ppa:ondrej/php
sudo apt-get install -y php8.1-fpm php8.1 libapache2-mod-php8.1 php8.1-cli php8.1-common php8.1-mysql php8.1-zip php8.1-gd php8.1-mbstring php8.1-curl php8.1-xml php8.1-bcmath php8.1-pgsql php8.1-redis redis-server
sudo a2enmod actions fcgid alias proxy_fcgi
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt-get install -y nodejs
sudo apt -y install gcc g++ make
sudo npm install -g @quasar/cli
sudo apt -y install vim bash-completion wget
# wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo tee /etc/apt/trusted.gpg.d/pgdg.asc &>/dev/null
echo "deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
sudo apt update
sudo apt install postgresql-13 postgresql-client-13
sudo service postgresql start
sudo service redis-server start
sudo service apache2 reload
sudo service apache2 restart
```

instalmos [composer](https://getcomposer.org/download/) 

Clonamos el repositorio en nuestro home
entramos a las carpeta `~/contratos/backend` y ejecutamos

```sh
composer install
php artisan db:seed --class=WebsocketClientSeeder 
cp .env.example .env
php artisan k:g
```
