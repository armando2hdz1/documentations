# SSH - Generate public key from private key

En la consola de linux tener la llave privada

```sh
ls -la 
```

![PrivKey](./img/ls_key.png)

Ejecutar los siguientes comandos

```sh
ssh-keygen -y -f ./ecs-redccar-630526c69a067d53b40c8981.pem > ecs-redccar-630526c69a067d53b40c8981.pem.pub

## Nos mostrara la llave publica
cat ./ecs-redccar-630526c69a067d53b40c8981.pem.pub
```

La salida del comando `cat` la copiamos, nos vamos al servidor y abrimos el archivo `~/.ssh/authorized_keys` y pegamos la llave publica